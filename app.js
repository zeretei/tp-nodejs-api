const express = require("express");
const mongoose = require("mongoose");
const config = require("./.config.json");

const app = express();

// database
const conn = `mongodb+srv://
    ${config.db.username}:${config.db.password}@cluster0.fhb0i.mongodb.net/
    ${config.db.dbname}?retryWrites=true&w=majority`;

mongoose.connect(conn, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.once("open", () => {
  console.log("connected to mongodb");
});

// middlewares
app.use(express.urlencoded());

// routes
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// starting server
app.listen(3000, console.log("listen.."));
